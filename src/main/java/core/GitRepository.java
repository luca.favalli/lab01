package core;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class GitRepository {
	private String repoPath;
	
	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}

	public String getHeadRef() {
		String path = this.repoPath + "/HEAD";
		String read = "";
		try {
			BufferedReader head = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
			read = head.readLine();
			read = read.replaceAll("^ref: ", "");		
			head.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return read;
	}

	public String getRefHash(String artifact) {
		String path = this.repoPath + "/" + artifact;
		String read = "";
		try {
			BufferedReader head = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
			read = head.readLine();	
			head.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return read;
	}
	
	
}
